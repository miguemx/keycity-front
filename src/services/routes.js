import Main from "../Pantallas/Main"
import LogIn from "../Pantallas/LogIn";
import ReportesEventos from "../Pantallas/ReportesEventos";
import Reportes from "../Pantallas/Reportes";

export const rutas = [
    
    { ruta: '/', component: <LogIn /> },
    { ruta: '/luminarias', component: <Main />, isPrivate: true },
    { ruta: '/reportes/moderacion', component: <ReportesEventos />},
    { ruta: '/reportes', component: <Reportes />}
    
];