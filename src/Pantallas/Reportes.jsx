import TableReportes from '../components/Tables/Reportes/TableReportes';
import MenuModulos from '../layout/MenuModulos/MenuModulos';
import TopBar from '../layout/TopBar/TopBar';

import './ReportesEventos.scss';
import 'semantic-ui-css/semantic.min.css'

const Reportes = () => {
	return (
		<>
		<div className="App">
		<TopBar titulo={"Reportes"}/>
			<div className="work-area">
				<MenuModulos />
				<TableReportes/>
			</div>
		</div>
		</>
	);
}

export default Reportes;