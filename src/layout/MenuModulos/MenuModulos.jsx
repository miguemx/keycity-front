import React, { useState, useEffect } from "react";
import classnames from "classnames";
import MapGeneral from "../../components/MapGeneral/MapGeneral";

import axios from 'axios'
import { Url } from '../../constants/global';

import LightIcon from '@mui/icons-material/Light';
import ListAltIcon from '@mui/icons-material/ListAlt';
import MapIcon from '@mui/icons-material/Map';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import AddBoxIcon from '@mui/icons-material/AddBox';

import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import Logout from '@mui/icons-material/Logout';
import TrafficIcon from '@mui/icons-material/Traffic';
import SensorsIcon from '@mui/icons-material/Sensors';
import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import SearchIcon from '@mui/icons-material/Search';

import RuleIcon from '@mui/icons-material/Rule';

import { useNavigate} from "react-router-dom";


import "./MenuModulos.scss"
import { Box, Button, IconButton, Modal, TextField } from "@mui/material";

const MenuModulos = () => {
    const token = localStorage.getItem('token');

    const [openModal, setopenModal] = useState(false)

    const [showMap, setshowMap] = useState(false)
    const [showCanal, setshowCanal] = useState(false)
    const [showFalla, setshowFalla] = useState(false)

    const [dataluminaria, setdataluminaria] = useState([])
    const [canales, setCanales] = useState([])
    const [selectcanal, setselectcanal] = React.useState('');
    const [fallas, setfallas] = useState([])
    const [selectfalla, setselectfalla] = React.useState('');

    const Navigate = useNavigate();

    useEffect(() =>{
    }, [])

    const navigatetoLuminarias = () => {
        Navigate("/luminarias");
    }

    const navigateToReportsEvents = () => {
        Navigate("/reportes/moderacion")
    }

    const navigateToReports = () => {
        Navigate("/reportes")
    }

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);

    const MenuReporteEvento = (event) => {
      setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    const abrirModal = () => {
        //Navigate("/reportes/moderacion")
        setopenModal(true)
        setshowMap(true);
        axios.get(Url + 'canales', {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            setCanales(res.data);
        })
        .catch(err => console.log(err))

        axios.get(Url + 'fallas?tipo=luminaria', {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            setfallas(res.data);
        })
        .catch(err => console.log(err))
    }

    const closeModal = () => {
        setopenModal(false)
        setdataluminaria([])
        setshowMap(false)
        setshowCanal(false)
        setshowFalla(false)
        setselectcanal('')
        setselectfalla('')
    }
    
    const handleChangeSelect = (event) => {
        setshowFalla(true)
        setselectcanal(event.target.value)
    }

    const handleChangeFalla = (event) => {
        setselectfalla(event.target.value)
    }

    const styleModal = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '80%',
        height: '80%',
        bgcolor: 'background.paper',
        border: '2px solid #000',
        p: 4
        //overflow: 'auto'
    };

    const MenuProps = {
        style: {
          maxHeight: 250,
      },
    };

    return(
        <><div className={classnames("Menu")}>
            <button className={classnames("Menu-item")} onClick={navigatetoLuminarias}>
                {/*<FontAwesomeIcon icon={faBolt} />*/}
                <LightIcon fontSize="small">
                </LightIcon>
                <span className="Menu-text">Luminarias</span>
            </button>

            <button className={classnames("Menu-item")}>
                <TrafficIcon />
                <span className="Menu-text">Semáforos</span>
            </button>

            <button className={classnames("Menu-item")}>
                <SensorsIcon />
                <span className="Menu-text">Sensores</span>
            </button>          

            <button className={classnames("Menu-item")} onClick={MenuReporteEvento}>
                <RuleIcon />
                <span className="Menu-text">Moderación</span>
            </button>  

            <button className={classnames("Menu-item")} onClick={navigateToReports}>
                <ReportGmailerrorredIcon />
                <span className="Menu-text">Reportes</span>
            </button>  
            
        </div>
        
        <React.Fragment>
            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                        ml: 6.5,
                        mt: -3.5,
                        '& .MuiAvatar-root': {
                            width: 35,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&:before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 12,
                            left: -4,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem onClick={navigateToReportsEvents}>
                    <ListItemIcon>
                        <ListAltIcon/>
                    </ListItemIcon>
                    Listado
                </MenuItem>
                <MenuItem onClick={() => abrirModal()}>
                    <ListItemIcon>
                        <AddBoxIcon/>
                    </ListItemIcon>
                    Crear Reporte
                </MenuItem>
                <Divider />
                <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                        <Logout fontSize="small" />
                    </ListItemIcon>
                    Salir
                </MenuItem>
            </Menu>
        </React.Fragment>

        <Modal
        open={openModal}
        onClose={closeModal}>
            <Box sx={{...styleModal, flexDirection:"column"}}>
                <h3 style={{textAlign:"center"}}>Crear Reporte</h3>
                
                <div style={{display:"flex"}}>    
                    <div style={{flex:3, height:400, marginRight:30}}>
                    <MapGeneral setinfoLuminaria={setdataluminaria} showButton={setshowCanal}
                    caso={2} /> 
                    Seleccione un punto para ver las Luminarias
                    <br/>
                    <br/>
                    <Button variant="contained" color="error" onClick={closeModal}>
                        Cancelar
                    </Button>
                    </div>

                    <div style={{flex:1}}>
                        <p>PDL: {dataluminaria.pdl_id||"--"}</p>
                        {showCanal &&
                        <FormControl fullWidth>
                            Canal:
                            <Select
                                id="Canal"
                                value={selectcanal}
                                MenuProps={MenuProps}
                                onChange={handleChangeSelect}
                            >
                            {canales.map((canal, index) => (
                                <MenuItem key={index} value={canal.id}>{canal.nombre}</MenuItem>
                            ))}
                            </Select>
                        </FormControl>
                        }
                        <p/>
                        {showFalla &&
                        <FormControl fullWidth>
                            Falla:
                            <Select
                                id="Falla"
                                value={selectfalla}
                                MenuProps={MenuProps}
                                onChange={handleChangeFalla}
                            >
                            {fallas.map((falla, index) => (
                                <MenuItem key={index} value={falla.id}>{falla.nombre}</MenuItem>
                            ))}
                            </Select>
                        </FormControl>
                        }
                        <br/>
                        <Button variant="contained" color="error" onClick={closeModal}>
                            Cancelar
                        </Button>
                    </div>
                </div>
            </Box>
        </Modal>
        </>


        
    );

}

export default MenuModulos;