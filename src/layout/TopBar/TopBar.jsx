import React, { useState, useRef } from "react";
import classnames from "classnames";

import SettingsIcon from '@mui/icons-material/Settings';
import PersonIcon from '@mui/icons-material/Person';
import LogoutIcon from '@mui/icons-material/Logout';

import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import icon from '../../assets/icon.png'

import { useNavigate} from "react-router-dom";

import "./TopBar.scss";

function onKeyDownHandler(event) {

    let $estado = document.getElementById("Estado");

    if (event.key === "Enter") {
        console.log("Termino de escribir");
        alert("Estamos buscando..." + $estado.value);
    }
    //console.log("key pressed ",  String.fromCharCode(event.keyCode));
}

const TopBar = ({titulo}) => {
 
    const selectRef = useRef(null)

    const [age, setAge] = React.useState(1);

    const handleChange = (event) => {
        setAge(event.target.value);
    };
    
    const [isOpen, setIsOpen] = useState(true);

    const buttonRef = useRef(null);

    const Navigate = useNavigate();

    const handleClick = () => {
        setIsOpen(!isOpen)
        buttonRef.current.blur();
    };

    const salir = () => {
        localStorage.clear();
        Navigate("/");
    }

    return (
        <>
            <div className="TopBar">

                <div style={{display:"flex", flexDirection:"row", marginTop:-22, height:120, width: 150, 
                        justifyContent:"space-between"}}>
                    <img src={icon} 
                    width={100}
                    alt="Key City"/>
                </div>

                <div style={{marginTop:'0.6%', marginLeft:'-65%'}}>
                    <h2>{titulo}</h2>
                </div>

                <div className="ButtonA">
                    {/*
                    <span style={{paddingTop: '1.7%', paddingRight: '1%'}}>Proyecto:</span>
                    <FormControl fullWidth size="small" 
                    sx={{ width: 200, paddingRight: '2%', paddingTop: '1.5%'}}>
                        <Select
                            onClose={() => {
                                setTimeout(() => {
                                document.activeElement.blur();
                                }, 0);
                            }}
                            id="demo-simple-select"
                            value={age}
                            onChange={handleChange}
                            >
                            <MenuItem value={1}>Perseo</MenuItem>
                            <MenuItem value={2}>Resaca</MenuItem>
                            <MenuItem value={3}>Playa</MenuItem>
                        </Select>
                    </FormControl>

                    <div className={classnames("ui left icon input")} style={{width: 200, height: 25, marginTop:8}}>
                        <input id="Estado" type="text"  placeholder="PDL" onKeyDownCapture={onKeyDownHandler}/>
                        <i aria-hidden="true" className={classnames("search icon")}></i>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                        */}

                    <button className={classnames("mini circular ui button")} type="button" onClick={handleClick} 
                    style={{width: 60, height: 50, backgroundColor: 'rgba(52, 52, 52, 0.0)', marginRight: -15}} 
                    ref={buttonRef}>
                        <PersonIcon/>
                    </button>
                    &nbsp;&nbsp;

                    {isOpen &&
                    <button className={classnames("mini circular ui button")}
                    style={{width: 60, height: 50, backgroundColor: 'rgba(52, 52, 52, 0.0)', marginRight: -15}}>
                        <SettingsIcon/>
                    </button>}
                    &nbsp;&nbsp;

                    <button className={classnames("mini circular ui button")} onClick={salir}
                    style={{width: 60, height: 50, backgroundColor: 'rgba(52, 52, 52, 0.0)', marginRight: -15}}>
                        <LogoutIcon />
                    </button>
                </div>
            </div>
        </>
    );
}

export default TopBar;