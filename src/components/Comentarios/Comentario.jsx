import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import Timeline from '@mui/lab/Timeline';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TextField from '@mui/material/TextField';

import React, { useRef, useState, useEffect} from "react";
import axios from 'axios'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { blue } from '@mui/material/colors';

const Comentarios = ({datacoments}) => {
    
    const [datoscomentarios, setdatoscomentarios] = useState([])

    const diferencia = (stringfecha) => {
        const today = new Date();
        const datafecha = new Date(stringfecha);

        var diaEnSeg = 1000;
        var mensajefinal = "";

        var dif = ((today.getTime() - datafecha.getTime())/diaEnSeg)/60

        if (dif<60) {
            mensajefinal = "Hace: " + dif.toFixed(0) + "min";
        }else if((dif=>60) && (dif<1440))
        {
            dif=dif/60
            mensajefinal = "Hace: " + dif.toFixed(0) + "hrs";

        }else if(dif=>1440){
            dif=dif/1440

            if(dif.toFixed(0) <=2){
            mensajefinal = "Hace: " + dif.toFixed(0) + "día(s)";
            }else{
                mensajefinal = ""
            }
        }

        return mensajefinal
    }

    useEffect(() => {
        setdatoscomentarios([]);

        datacoments.map(element => {
            setdatoscomentarios((array) => {
                return [...array,{
                    Usuario: element.username,
                    Comentario: element.comentario,
                    fecha: element.created_at,
                    diferencia: diferencia(element.created_at)
                }];
            })
        });
    })

    return ( 
        <div>
            <Box component="div">
                <TextField id="outlined-basic" variant="outlined" placeholder="Comentario" value = {""}
                    sx={{width:360, padding:1, marginLeft:-1}} color="success" //onChange={handleChange}
                    autoFocus={true}/>
                <Button variant="outlined" color="success" //onClick={addItem}
                    sx={{marginTop:1, height:52}}>
                    <AddCircleOutlineIcon sx={{marginRight:1}}/>
                        Crear
                </Button>
                <Timeline style={{marginLeft:-475, marginRight: -20}}>
                    {datoscomentarios.map((Comentario, index) => (
                    <TimelineItem key={index}>
                        <TimelineSeparator>
                            <TimelineDot variant="outlined" color="primary"/>
                            <TimelineConnector />
                        </TimelineSeparator>
                        <TimelineContent>          
                            <div style={{display:"flex", flexDirection:"row", justifyContent:"space-between"}}>
                                <div style={{textAlign:"left"}}>
                                    <AccountCircleIcon sx={{marginBottom:-0.5, marginRight:1, color:blue[600]}}/>
                                    <Typography variant="h6" component="span">
                                        {Comentario.Usuario||""}
                                    </Typography>
                                    <Typography>{Comentario.Comentario||""}</Typography>
                                </div>
                                <div style={{textAlign:"right"}}>
                                    <Typography>
                                        {Comentario.diferencia === "" ? 
                                        Comentario.fecha||"" : Comentario.diferencia||""}
                                        </Typography>
                                </div>
                            </div>
                        </TimelineContent>
                    </TimelineItem>
                        ))}
                </Timeline>
            </Box>
        </div>
    );
}

export default Comentarios;