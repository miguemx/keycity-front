import React, { useState} from "react";
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

import ListItemIcon from '@mui/material/ListItemIcon';

import classNames from "classnames";

const Panel = ({closePanel, children, dato, icono, top}) => {

    const [valorTab, setvalorTab] = useState("Informacion");
    
    const style = {
        position: 'absolute',
        //top: '0.5%',
        //left: '0.5%',
        width: '45%',
        //height: '91%',
        top: {top},
        bottom: 0,
        bgcolor: 'white',
        boxShadow: '10px 1px 8px -1px rgba(0,0,0,0.3)',
        p: 1.5,
        overflow: 'auto',
        textAlign:'center',
      };

    //Acceder a evento dependiendo del tab que se haya seleccionado
    const handleChange2 = (event, newValue) => {
        setvalorTab(newValue);
      /*if (newValue === '2') {
      }else if(newValue === '3'){
      }*/
    };

    return (
        <>       
        <div className={classNames("Panel")}>
            <Box sx={{...style, zIndex:3}}>
                <div style={{
                        display: 'flex',
                        justifyContent:'space-between',
                        alignItems:'center',
                        top: 0}}>
                    <div>
                        <Typography id="modal-modal-title" variant="h6">
                            <ListItemIcon style={{marginRight:-25}}>
                                {icono}
                            </ListItemIcon>
                            {dato}
                        </Typography>
                    </div>
                    <div>
                        <button className={classNames("mini circular ui button")}
                            style={{width: 40, backgroundColor: "transparent"}} onClick={closePanel}>
                        <span>X</span> 
                        </button>
                    </div>
                </div>
            <Divider style={{marginBottom:10, marginTop:10}}/>

                {children}

            </Box>
        </div>
        </>
    );
}

export default Panel;