//import * as React from 'react';
import axios from 'axios'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useEffect, useState, useRef } from "react";
import Button from '@mui/material/Button';
import MapIcon from '@mui/icons-material/Map';
import IconButton from '@mui/material/IconButton';
import ZoomOutMapIcon from '@mui/icons-material/ZoomOutMap';
import FilterAltOffIcon from '@mui/icons-material/FilterAltOff';

/*import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';*/

import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import Popover from '@mui/material/Popover';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';


import FirstPageIcon from '@mui/icons-material/FirstPage';


import "./TableReportesEventos.scss";

import classNames from "classnames";
import Panel from '../../Panel Lateral/Panel';
import Reporte from './ReporteEvento/ReporteEvento';
import { Box } from '@mui/material';

import { Url } from '../../../constants/global';
import { useNavigate } from "react-router-dom";
import LoaderIndicator from '../../../layout/LoaderIndicator/LoaderIndicator';

const TableReportes = () => {
    const Navigate = useNavigate();

    const [data, SetData] = useState([])
    const [users, setUsers] = useState([])
    const token = localStorage.getItem('token');

    const refTextFolioSeg = useRef(null);
    const refTextPDL = useRef(null);
    const refTextNombre = useRef(null);

    const [disabled, setdisabled] = useState(false)

    const [openPanel, setopenPanel] = useState(false)
    const [Reporteporid, setReporteporid] = useState([])
    const [showFilter, setShowFilter] = useState(false)
    const [showFilterPDL, setShowFilterPDL] = useState(false)
    const [showFilterNombre, setShowFilterNombre] = useState(false)
    const [anchorEl, setAnchorEl] = React.useState(null);

    const [nombreBuscado, setNombreBuscado] = useState("")
    const [banderaFolio, setbanderaFolio] = useState(true)
    let   [pdlbuscado, setpdlbuscado] = useState("")
    let   [usuariobuscado, setusuariobuscado] = useState("")
    const [banderaPDL, setbanderaPDL] = useState(true)
    const [totalRegistros, settotalRegistros] = useState(0)

    const [ isCharging, setisCharging ] = useState(false);

    let FirstPage = "";
    //const [age, setAge] = React.useState(50);

    /*const handleChangeS = (event) => {
        var perpage = event.target.value
        setAge(event.target.value);

        axios.get(Url + 'reporte_eventos?items='+perpage, {
            headers: {
                Authorization : token,
            }
          })
        .then(res =>  {
            SetData(res.data.data);
            settotalRegistros(res.data.total)
          })
        .catch(err => console.log(err))
      };*/

    const cargarReportes = () => {
        axios.get(Url + 'reporte_eventos', {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            SetData(res.data.data);
            settotalRegistros(res.data.total)
          })
        .catch(err => console.log(err))
    }

    const cargarReportesFiltrados = () => {
        //console.log(pdlbuscado +"-"+usuariobuscado)
        axios.get(Url + 'reporte_eventos?pdl='+(pdlbuscado||"")+'&usuario='+ usuariobuscado, {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            SetData(res.data.data);
            settotalRegistros(res.data.total)
          })
        .catch(err => console.log(err))
    }

    useEffect(() =>{
        if(token === null){
            Navigate("/")
        }else{
            setisCharging(true)
            axios.get(Url + 'reporte_eventos', {
                headers: {
                    Authorization : token,
                }
            })
            .then(res =>  {
                SetData(res.data.data);
                settotalRegistros(res.data.total)
                FirstPage = res.data.first_page_url
                //console.log(FirstPage)
                setisCharging(false)
            })
            .catch(err => {
                setisCharging(false)
                console.log(err)})

            axios.get(Url + 'users?entidad=reporteseventos', {
                headers: {
                    Authorization : token,
                }
            })
            .then(res =>  {
                setUsers(res.data);
            })
            .catch(err => console.log(err))
        }
    }, [])

    const mandarMaps = (latitud, longitud) => {
        window.open("https://www.google.es/maps?q="+latitud+","+longitud, "_blank");
    }

    const isNull = (dato) =>{
        let editado = ""
        if(dato === null){
            editado = "---"
        }else{
            editado = dato
        }

        return editado
    }

    const abrirPanel = (reporte) => {
        setReporteporid(reporte)
        //console.log(Reporteporid.id)
        setopenPanel(true);
        //console.log(openPanel)
    }

    const cerrarPanel = () => {
        setopenPanel(false);
    }

    //Filtro Folio Seguimiento
    const abrirPop = (event) => {
        setAnchorEl(event.currentTarget);
        setShowFilter(true);

        setTimeout(() => {
            refTextFolioSeg.current.focus();
          }, 0);
    }

    const cerrarPop = () => {
        setShowFilter(false);
    }

    const onKeyDownHandler = (event) => {

        let $estado = document.getElementById("FolioSeg");
    
        if (event.key === "Enter") {
            if($estado.value === ""){
                cargarReportes();
            }else{
                axios.get(Url + 'reporte_eventos/'+ $estado.value, {
                    headers: {
                        Authorization : token,
                    }
                })
                .then(res =>  {
                    const data = [
                        res.data.data
                    ]
                    SetData(data);
                    settotalRegistros(1)
                    setbanderaFolio(false)
                    setbanderaPDL(true)
                    setNombreBuscado("")
                    setdisabled(true)
                    cerrarPop()
                  })
                .catch(err => console.log(err))
            }
        //console.log($estado);
        }
        //console.log("key pressed ",  String.fromCharCode(event.keyCode));
    }

    //Filtro Nombre
    const abrirPopNombre = (event) => {
        setAnchorEl(event.currentTarget);
        setShowFilterNombre(true);

        setTimeout(() => {
            refTextNombre.current.focus();
          }, 0);
    }

    const cerrarPopNombre = () => {
        setShowFilterNombre(false);
    }

    const handleChange = (valor) => {
        if(valor === null){
            setNombreBuscado("")
            usuariobuscado=""
            cargarReportesFiltrados();
        }else{
            setNombreBuscado(valor.id||"")
            setusuariobuscado(valor.id)
            axios.get(Url + 'reporte_eventos?pdl='+pdlbuscado+'&usuario='+ valor.id, {
                headers: {
                    Authorization : token,
                }
            })
            .then(res =>  {
                SetData(res.data.data);
                settotalRegistros(res.data.total)
                if(res.data.data.length !== 0){
                    cerrarPopNombre();
                }
              })
            .catch(err => console.log(err))
        }
    }

    //Filtro PDL
    const abrirPopPDL = (event) => {
        setAnchorEl(event.currentTarget);
        setShowFilterPDL(true);

        setTimeout(() => {
            refTextPDL.current.focus();
          }, 0);
    }

    const cerrarPopPDL = () => {
        setShowFilterPDL(false);
    }

    const handleChangePDL = (event) => {
        setpdlbuscado(event.target.value)
        //console.log(event.target.value)
    }

    const onKeyDownHandlerPDL = (event) => {

        let $PDL = document.getElementById("PDL");
    
        if (event.key === "Enter") {
            if($PDL.value === ""){
                cargarReportes();
            }else{
                axios.get(Url + 'reporte_eventos?pdl='+(pdlbuscado)+'&usuario='+ usuariobuscado, {
                    headers: {
                        Authorization : token,
                    }
                })
                .then(res =>  {
                    SetData(res.data.data);
                    settotalRegistros(res.data.total)
                    setbanderaPDL(false)
                    if(res.data.data.length !== 0){
                        cerrarPopPDL();
                    }
                  })
                .catch(err => console.log(err))

            }
        //console.log($estado);
        }
        //console.log("key pressed ",  String.fromCharCode(event.keyCode));
    }

    const limpiarflitroNombre = () => {
        setNombreBuscado("")
        usuariobuscado = ""
        cargarReportesFiltrados();
    }

    const firstpage = () => {
        /*axios.get(FirstPage, {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            SetData(res.data.data);
            settotalRegistros(res.data.total)
          })
        .catch(err => console.log(err))*/
    }

    const styleCell = {
        color: 'white',
        textAlign: "center",
        backgroundColor: "#237f65",
    }

    const styleWihtR = {
        color: '#808B96',
        textAlign: 'center',
    }

    const styleWithoutR = {
        color: 'black',
        textAlign: 'center',
    }

    return (
        <div className={classNames("TableReportesEventos")}>
            {openPanel &&
                <Panel 
                dato={"Folio de Seguimiento: " + Reporteporid.id} 
                closePanel={cerrarPanel} 
                icono={<ReportGmailerrorredIcon/>}
                top={'50px'}
                >
                    <Reporte dataReporte={Reporteporid} cerrarPanel={cerrarPanel} cargareportes={cargarReportes}/>
                </Panel>
            }
            <TableContainer>
                <Table stickyHeader={!openPanel} size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell sx={styleCell}>
                            <div style={{ display: 'flex', justifyContent:'center', alignItems:'center'}}>
                                Folio de Seguimiento
                                    {banderaFolio ?
                                    <IconButton onClick={abrirPop} 
                                    sx={openPanel ? {position:'inherit'}:{position:'relative', color:'white'}}>
                                        <FilterAltIcon />
                                    </IconButton>
                                    :
                                    <IconButton sx={openPanel ? {position:'inherit'}:{position:'relative', color:'white'}}
                                    onClick={() => {setbanderaFolio(true)
                                                    cargarReportes()
                                                    setdisabled(false)}}
                                    >
                                        <FilterAltOffIcon/>
                                    </IconButton>
                                    }  
                                    <Popover
                                        open={showFilter}
                                        anchorEl={anchorEl}
                                        onClose={cerrarPop}
                                        anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'left',
                                        }}
                                        >
                                        <Box p={1} component="div">
                                            <TextField id="FolioSeg" sx={{width:'100%'}} variant="standard" 
                                            inputRef={refTextFolioSeg}
                                            //onChange={handleChangeFolio} 
                                            onKeyDownCapture={onKeyDownHandler}
                                            />
                                        </Box>
                                    </Popover>
                            </div>
                            </TableCell>
                            <TableCell sx={styleCell}>
                            <div style={{ display: 'flex', justifyContent:'center', alignItems:'center'}}>
                                        PDL
                                        {banderaPDL ?
                                            <IconButton onClick={abrirPopPDL} 
                                            sx={openPanel ? {position:'inherit'}:{position:'relative', color:'white'}}
                                                disabled={disabled}>
                                                <FilterAltIcon/>
                                            </IconButton>   
                                        :
                                        <IconButton sx={openPanel ? {position:'inherit'}:{position:'relative', color:'white'}}
                                        onClick={() => {setbanderaPDL(true)
                                                        pdlbuscado=""
                                                        setpdlbuscado("")
                                                        cargarReportesFiltrados()}}
                                        >
                                            <FilterAltOffIcon/>
                                        </IconButton>
                                        }     
                                        <Popover
                                        open={showFilterPDL}
                                        anchorEl={anchorEl}
                                        onClose={cerrarPopPDL}
                                        anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'left',
                                        }}
                                        >
                                        <Box p={1} component="div">
                                            <TextField id="PDL" sx={{width:'100%'}} variant="standard" 
                                            onChange={handleChangePDL} inputRef={refTextPDL}
                                            onKeyDownCapture={onKeyDownHandlerPDL}
                                            />
                                        </Box>
                                    </Popover>      
                            </div>
                            </TableCell>
                            <TableCell sx={styleCell}>Coordenadas</TableCell>
                            <TableCell sx={styleCell}>Calle</TableCell>
                            <TableCell sx={styleCell}>Colonia</TableCell>
                            <TableCell sx={styleCell}>Entre Calles</TableCell>
                            <TableCell sx={styleCell}>Referencia</TableCell>
                            <TableCell sx={styleCell}>
                                <div style={{ display: 'flex', justifyContent:'center', alignItems:'center'}}>
                                    Usuario
                                    {nombreBuscado === (null||"") ?
                                    <IconButton onClick={abrirPopNombre} sx={{color:'white'}}
                                        disabled={disabled}>
                                        <FilterAltIcon/>
                                    </IconButton>
                                    :
                                    <IconButton sx={{color:'white'}}
                                                onClick={limpiarflitroNombre}>
                                        <FilterAltOffIcon/>
                                    </IconButton>
                                    }
                                    <Popover
                                        open={showFilterNombre}
                                        anchorEl={anchorEl}
                                        onClose={cerrarPopNombre}
                                        anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'left',
                                        }}
                                        >
                                        <Box p={1} component="div" height={200}>
                                            <Autocomplete
                                            disablePortal
                                            id="combo-box-demo"
                                            onChange={(event, value) => handleChange(value)}
                                            options={users}
                                            getOptionLabel={(option) => option.nombre_completo}
                                            sx={{ width: 200 }}
                                            renderInput={(params) => <TextField {...params} 
                                                                        inputRef={refTextNombre}
                                                                        id="nombreBuscado"
                                                                        value={nombreBuscado}
                                                                        variant="standard"/>}
                                            />
                                        </Box>
                                    </Popover>
                                </div>
                            </TableCell>
                            <TableCell sx={styleCell}>Fecha Registro</TableCell>
                            <TableCell sx={styleCell}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody >
                        {data.map((reporte, index) => (
                            <TableRow key={index}>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>{reporte.id}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>{reporte.etiqueta||"--"}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR} style={{textAlign:'left' }}>
                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <div style={{ marginTop: '3.5%' }}>
                                            {reporte.latitud}, {reporte.longitud}
                                        </div>
                                        <IconButton sx={openPanel ? {position:'inherit'}:{position:'relative'}}
                                            onClick={() => mandarMaps(reporte.latitud, reporte.longitud)}>
                                            <MapIcon />
                                        </IconButton>
                                    </div>
                                </TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>{isNull(reporte.calle)}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>{isNull(reporte.colonia)}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR} style={{width:100}}>{isNull(reporte.entrecalles)}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>{reporte.referencia||"---"}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>
                                    {reporte.usuario.nombre} {reporte.usuario.ap_paterno} {reporte.usuario.ap_materno}
                                </TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>{reporte.created_at}</TableCell>
                                <TableCell sx={reporte.reporte!==null ? styleWihtR:styleWithoutR}>
                                    <Button variant="outlined" 
                                    sx={{borderColor: 'black', color:'black'}}
                                    onClick={() => abrirPanel(reporte)}>
                                        <ZoomOutMapIcon />
                                    </Button>
                        </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <br/>
            <br/>
            {!openPanel &&
                <>
                <footer style={{display:"flex" ,justifyContent:"space-between", width:1200}}>
                    Total de Registros: {totalRegistros}
                <IconButton size="small" onClick={firstpage}>
                    <FirstPageIcon/>
                </IconButton>
                </footer>
                {/*<footer style={{display: 'flex', alignItems:'center', justifyContent:'space-between', width:1200}}>

                    <div>
                        Total de Registros: {totalRegistros}
                    </div>

                    <div style={{display:'flex' ,alignItems:'center'}}>
                        <FormControl fullWidth size="small" 
                        sx={{ width: 90, marginRight: 5, p:0.5}}>
                            <Select
                                onClose={() => {
                                    setTimeout(() => {
                                    document.activeElement.blur();
                                    }, 0);
                                }}
                                id="demo-simple-select"
                                value={age}
                                onChange={handleChangeS}
                                >
                                <MenuItem value={5}>5</MenuItem>
                                <MenuItem value={10}>10</MenuItem>
                                <MenuItem value={50}>50</MenuItem>
                            </Select>
                        </FormControl>
                        
                        <label style={{width:40, marginRight:15}}>1-{age}</label>
                    </div>
                </footer>
                            */}
                </>
            }
        { isCharging && <LoaderIndicator /> }
        </div>
    );
}

export default TableReportes;         