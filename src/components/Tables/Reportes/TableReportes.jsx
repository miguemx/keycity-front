//import * as React from 'react';
import axios from 'axios'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useEffect, useState, useRef } from "react";
import Button from '@mui/material/Button';
import MapIcon from '@mui/icons-material/Map';
import IconButton from '@mui/material/IconButton';
import ZoomOutMapIcon from '@mui/icons-material/ZoomOutMap';
import FilterAltOffIcon from '@mui/icons-material/FilterAltOff';
import Chip from '@mui/material/Chip';

import CampaignIcon from '@mui/icons-material/Campaign';

import FirstPageIcon from '@mui/icons-material/FirstPage';

/*import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';*/

import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import Popover from '@mui/material/Popover';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

import classNames from "classnames";
import Panel from '../../Panel Lateral/Panel';
import Reporte from '../Reportes/Reportes/Reporte';
import { Box } from '@mui/material';

import './TableReportes.scss'
import { Url } from '../../../constants/global';
import { useNavigate } from "react-router-dom";
import LoaderIndicator from '../../../layout/LoaderIndicator/LoaderIndicator';

const TableReportes = () => {
    const Navigate = useNavigate();

    const [data, SetData] = useState([])
    const [users, setUsers] = useState([])
    const token = localStorage.getItem('token');

    const refTextFolioSeg = useRef(null);
    const refTextPDL = useRef(null);
    const refTextNombre = useRef(null);

    const [disabled, setdisabled] = useState(false)

    const [openPanel, setopenPanel] = useState(false)
    const [Reporteporid, setReporteporid] = useState([])
    const [showFilter, setShowFilter] = useState(false)
    const [showFilterPDL, setShowFilterPDL] = useState(false)
    const [showFilterNombre, setShowFilterNombre] = useState(false)
    const [anchorEl, setAnchorEl] = React.useState(null);

    const [nombreBuscado, setNombreBuscado] = useState("")
    const [banderaFolio, setbanderaFolio] = useState(true)
    let   [pdlbuscado, setpdlbuscado] = useState("")
    let   [usuariobuscado, setusuariobuscado] = useState("")
    const [banderaPDL, setbanderaPDL] = useState(true)
    const [totalRegistros, settotalRegistros] = useState(0)

    const [ isCharging, setisCharging ] = useState(false);
    //const [age, setAge] = React.useState(50);

    const cargarReportes = () => {
        axios.get(Url + 'reportes', {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            SetData(res.data.data);
            settotalRegistros(res.data.total)
          })
        .catch(err => console.log(err))
    }

    const cambiar = (estatus) => {
        let nuevo = estatus

        if (estatus === "CREADO") {
            nuevo = "NUEVO"
        }else{
            nuevo = estatus
        }

        return nuevo
    }

    const stylerow = (estatus) => {
        switch (estatus) {
            case "CREADO":
                const styleCellRowCreado = {
                    color: 'black',
                    backgroundColor: "#2BEBEB",
                }

                return styleCellRowCreado;
        
            case "ASIGNADO":
                const styleCellRowAsignado = {
                    color: 'white',
                    backgroundColor: "#CDAE00",
                }
                return styleCellRowAsignado;

            case "EN PROCESO":
                const styleCellRowProceso = {
                    color: 'black',
                    backgroundColor: "#EBE82B",
                }
                return styleCellRowProceso;

            case "SOLUCIONADO":
                const styleCellRowSolucionado = {
                    color: 'white',
                    backgroundColor: "#2B88EB",
                }
                return styleCellRowSolucionado;
    
            case "TERMINADO":
                const styleCellRowTerminado = {
                    color: 'white',
                    backgroundColor: "#28B463",
                }
                return styleCellRowTerminado;

            case "RECHAZADO":
                const styleCellRowRechazado = {
                    color: 'black',
                }
                return styleCellRowRechazado;

            default:
                const styleCellRowN = {
                    color: 'black',
                    textAlign: 'center'
                }
                return styleCellRowN;
        }
    }

    const cargarReportesFiltrados = () => {
        console.log(pdlbuscado +"-"+usuariobuscado)
        axios.get(Url + 'reporte_eventos?pdl='+(pdlbuscado||"")+'&usuario='+ usuariobuscado, {
            headers: {
                Authorization : token,
            }
        })
        .then(res =>  {
            SetData(res.data.data);
            settotalRegistros(res.data.total)
          })
        .catch(err => console.log(err))
    }

    useEffect(() =>{
        if(token === null){
            Navigate("/")
        }else{
            setisCharging(true)
            axios.get(Url + 'reportes', {
                headers: {
                    Authorization : token,
                }
            })
            .then(res =>  {
                setisCharging(false)
                SetData(res.data.data);
                settotalRegistros(res.data.total)
            })
            .catch(err => {
                setisCharging(false)
                console.log(err)})

            axios.get(Url + 'users?entidad=reporteseventos', {
                headers: {
                    Authorization : token,
                }
            })
            .then(res =>  {
                setUsers(res.data);
            })
            .catch(err => console.log(err))
        }
    }, [])

    const mandarMaps = (latitud, longitud) => {
        window.open("https://www.google.es/maps?q="+latitud+","+longitud, "_blank");
    }

    const abrirPanel = (reporte) => {
        setReporteporid(reporte)
        setopenPanel(true);
    }

    const cerrarPanel = () => {
        setopenPanel(false);
    }

    //Filtro Folio Seguimiento
    const abrirPop = (event) => {
        setAnchorEl(event.currentTarget);
        setShowFilter(true);

        setTimeout(() => {
            refTextFolioSeg.current.focus();
          }, 0);
    }

    const cerrarPop = () => {
        setShowFilter(false);
    }

    const onKeyDownHandler = (event) => {

        let $estado = document.getElementById("FolioSeg");
    
        if (event.key === "Enter") {
            if($estado.value === ""){
                cargarReportes();
            }else{
                axios.get(Url + 'reporte_eventos/'+ $estado.value, {
                    headers: {
                        Authorization : token,
                    }
                })
                .then(res =>  {
                    const data = [
                        res.data.data
                    ]
                    SetData(data);
                    settotalRegistros(1)
                    setbanderaFolio(false)
                    setbanderaPDL(true)
                    setNombreBuscado("")
                    setdisabled(true)
                    cerrarPop()
                  })
                .catch(err => console.log(err))
            }
        //console.log($estado);
        }
        //console.log("key pressed ",  String.fromCharCode(event.keyCode));
    }

    //Filtro Nombre
    const abrirPopNombre = (event) => {
        setAnchorEl(event.currentTarget);
        setShowFilterNombre(true);

        setTimeout(() => {
            refTextNombre.current.focus();
          }, 0);
    }

    const cerrarPopNombre = () => {
        setShowFilterNombre(false);
    }

    const handleChange = (valor) => {
        if(valor === null){
            setNombreBuscado("")
            usuariobuscado=""
            cargarReportesFiltrados();
        }else{
            setNombreBuscado(valor.id||"")
            setusuariobuscado(valor.id)
            axios.get(Url + 'reporte_eventos?pdl='+pdlbuscado+'&usuario='+ valor.id, {
                headers: {
                    Authorization : token,
                }
            })
            .then(res =>  {
                SetData(res.data.data);
                settotalRegistros(res.data.total)
                if(res.data.data.length !== 0){
                    cerrarPopNombre();
                }
              })
            .catch(err => console.log(err))
        }
    }

    //Filtro PDL
    const abrirPopPDL = (event) => {
        setAnchorEl(event.currentTarget);
        setShowFilterPDL(true);

        setTimeout(() => {
            refTextPDL.current.focus();
          }, 0);
    }

    const cerrarPopPDL = () => {
        setShowFilterPDL(false);
    }

    const handleChangePDL = (event) => {
        setpdlbuscado(event.target.value)
        //console.log(event.target.value)
    }

    const onKeyDownHandlerPDL = (event) => {

        let $PDL = document.getElementById("PDL");
    
        if (event.key === "Enter") {
            if($PDL.value === ""){
                cargarReportes();
            }else{
                axios.get(Url + 'reporte_eventos?pdl='+(pdlbuscado)+'&usuario='+ usuariobuscado, {
                    headers: {
                        Authorization : token,
                    }
                })
                .then(res =>  {
                    SetData(res.data.data);
                    settotalRegistros(res.data.total)
                    setbanderaPDL(false)
                    if(res.data.data.length !== 0){
                        cerrarPopPDL();
                    }
                  })
                .catch(err => console.log(err))

            }
        //console.log($estado);
        }
        //console.log("key pressed ",  String.fromCharCode(event.keyCode));
    }

    const limpiarflitroNombre = () => {
        setNombreBuscado("")
        usuariobuscado = ""
        cargarReportesFiltrados();
    }

    const styleCell = {
        color: 'white',
        textAlign: "center",
        backgroundColor: "#237f65",
    }

    const styleWihtR = {
        color: '#808B96',
        textAlign: 'center',
    }

    const styleCellRow = {
        color: 'black',
        textAlign: 'center',
    }

    return (
        <div className={classNames("TableReportes")}>
            {openPanel &&
                <Panel 
                dato={"Folio de Reporte: " + Reporteporid.id} 
                closePanel={cerrarPanel} 
                icono={<ReportGmailerrorredIcon/>}
                top={'50px'}>
                    <Reporte dataReporte={Reporteporid} cerrarPanel={cerrarPanel} cargareportes={cargarReportes}/>
                </Panel>
            }
            <TableContainer>
                <Table stickyHeader={!openPanel} size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell sx={styleCell}>
                            <div style={{ display: 'flex', justifyContent:'center', alignItems:'center'}}>
                                Folio Reporte
                            </div>
                            </TableCell>
                            <TableCell sx={styleCell}>
                            <div style={{ display: 'flex', justifyContent:'center', alignItems:'center'}}>
                                        PDL   
                            </div>
                            </TableCell>
                            <TableCell sx={styleCell}>Ubicación PDL</TableCell>
                            <TableCell sx={styleCell}>Urgente</TableCell>
                            <TableCell sx={styleCell}>Estatus</TableCell>
                            <TableCell sx={styleCell}>Fecha Creación</TableCell>
                            <TableCell sx={styleCell}>Fecha Última Modificación</TableCell>
                            <TableCell sx={styleCell}></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody >
                        {data.map((reporte, index) => (
                            <TableRow key={index}>
                                <TableCell sx={styleCellRow}>{reporte.id}</TableCell>
                                <TableCell sx={styleCellRow}>{reporte.luminaria.pdl_id}</TableCell>
                                <TableCell sx={styleCellRow}>
                                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems:'center' }}>
                                        <div style={{ marginTop: '3.5%' }}>
                                            {reporte.luminaria.latitud}, {reporte.luminaria.longitud}
                                        </div>
                                        <IconButton sx={openPanel ? {position:'inherit'}:{position:'relative'}}
                                            onClick={() => mandarMaps(reporte.luminaria.latitud, 
                                                                    reporte.luminaria.longitud)}>
                                            <MapIcon />
                                        </IconButton>
                                    </div>
                                </TableCell>
                                <TableCell sx={styleCellRow}>
                                    {reporte.urgente === null ? reporte.urgente||"--" : <CampaignIcon/>}
                                </TableCell>
                                <TableCell sx={styleCellRow}>
                                    <Chip label={cambiar(reporte.estado)} 
                                    sx={[stylerow(reporte.estado), 
                                    {width:90, borderRadius:1.5, fontSize:"0.65rem", height:20, fontWeight:"bold"}]} 
                                    size="small" />
                                </TableCell>
                                <TableCell sx={styleCellRow}>{reporte.created_at}</TableCell>
                                <TableCell sx={styleCellRow}>{reporte.updated_at}</TableCell>
                                <TableCell>
                                    <Button variant="outlined" 
                                    sx={{borderColor: 'black', color:'black'}}
                                    onClick={() => abrirPanel(reporte)}>
                                        <ZoomOutMapIcon />
                                    </Button>
                        </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <br/>
            {!openPanel &&
                <>
                <footer>
                    Total de Registros: {totalRegistros}
                </footer>
                {/*<footer style={{display: 'flex', alignItems:'center', justifyContent:'space-between', width:1200}}>

                    <div>
                        Total de Registros: {totalRegistros}
                    </div>

                    <div style={{display:'flex' ,alignItems:'center'}}>
                        <FormControl fullWidth size="small" 
                        sx={{ width: 90, marginRight: 5, p:0.5}}>
                            <Select
                                onClose={() => {
                                    setTimeout(() => {
                                    document.activeElement.blur();
                                    }, 0);
                                }}
                                id="demo-simple-select"
                                value={age}
                                onChange={handleChangeS}
                                >
                                <MenuItem value={5}>5</MenuItem>
                                <MenuItem value={10}>10</MenuItem>
                                <MenuItem value={50}>50</MenuItem>
                            </Select>
                        </FormControl>
                        
                        <label style={{width:40, marginRight:15}}>1-{age}</label>
                    </div>
                </footer>
                            */}
                </>
            }
        { isCharging && <LoaderIndicator /> }
        </div>
    );
}

export default TableReportes;         