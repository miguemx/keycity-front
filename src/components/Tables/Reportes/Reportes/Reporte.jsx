import React, { useState} from "react";
import axios from 'axios'
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

//iconos Panel
import AlignHorizontalLeftIcon from '@mui/icons-material/AlignHorizontalLeft';
import AlignVerticalBottomIcon from '@mui/icons-material/AlignVerticalBottom';
import VillaIcon from '@mui/icons-material/Villa';
import SignpostIcon from '@mui/icons-material/Signpost';
import LightIcon from '@mui/icons-material/Light';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';

import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import MapIcon from '@mui/icons-material/Map';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import DoneOutlineIcon from '@mui/icons-material/DoneOutline';
import EmojiObjectsIcon from '@mui/icons-material/EmojiObjects';

import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import DvrIcon from '@mui/icons-material/Dvr';

import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import Modal from '@mui/material/Modal';

import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faRoad } from '@fortawesome/free-solid-svg-icons'
import {  faMagnifyingGlassLocation } from '@fortawesome/free-solid-svg-icons'

import "./Reporte.scss";
import { blue, green, grey, red } from "@mui/material/colors";
import { IconButton } from "@mui/material";
import { Url } from "../../../../constants/global";

const Reporte = ({dataReporte, cerrarPanel, cargareportes}) => {

    const MenuProps = {
          style: {
            maxHeight: 250,
        },
      };

    const token = localStorage.getItem('token');
    const MySwal = withReactContent(Swal);

    const [reporte, setReporte] = useState([])
    const [luminaria, setLuminaria] = useState([])
    const [openModal, setopenModal] = useState(false)
    const [cuadrilla, setcuadrilla] = React.useState(0)
    const [cuadrilla2, setcuadrilla2] = React.useState([]);

    if (reporte.id !== dataReporte.id) {
        setReporte(dataReporte);
        setLuminaria(dataReporte.luminaria)
    }

    function padTo2Digits(num) {
        return num.toString().padStart(2, '0');
      }
      
      function formatDate(date) {
        return (
          [
            date.getFullYear(),
            padTo2Digits(date.getMonth() + 1),
            padTo2Digits(date.getDate()),
          ].join('-') +
          ' ' +
          [
            padTo2Digits(date.getHours()),
            padTo2Digits(date.getMinutes()),
            padTo2Digits(date.getSeconds()),
          ].join(':')
        );
      }

    const handleChange = (event) => {
        setcuadrilla(event.target.value)
        setReporte(values => ({...values, ['cuadrilla']: event.target.value})) 
        
        /*const fechahoy = new Date();
        setReporte(values => ({...values, ['fecha_asignacion']: formatDate(fechahoy)}))*/
    };

    const mandarMaps = (reporte) => {
        window.open("https://www.google.es/maps?q="+reporte.latitud+","+reporte.longitud, "_blank");
    }

    const abrirSwalDelete = (FolioEliminar) => {
        MySwal.fire({
            title: <strong>Confirmación</strong>,
            showConfirmButton: false,
            showDenyButton: true,      
            denyButtonText: 'RECHAZAR',
            html: <p>Estás seguro de rechazar el Folio de Seguimiento: {FolioEliminar}?</p>,
            icon: 'question'
        })
        .then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isDenied) {
                axios.delete(Url + 'reporte_eventos/'+FolioEliminar, {
                headers: {
                    Authorization : token,
                }
                })
                .then(res =>  {
                    Swal.fire('Realizado!', '', 'success')
                    cerrarPanel();
                    cargareportes();
                })
                .catch(err => console.log(err))
            };
        })}

    const abrirModal = async () => {
        setcuadrilla(0)
        axios.get(Url + 'cuadrillas', {
            headers: {
                Authorization : token,
            }
            })
            .then(res =>  {
                setcuadrilla2(res.data.data)
            })
            .catch(err => console.log(err))
        setopenModal(true)
    }

    const closeModal = () => {
        setopenModal(false);
    }

    const asignacion = () => {
        const fechahoy = new Date();
        //setReporte(values => ({...values, ['fecha_asignacion']: formatDate(fechahoy)}))
        reporte.fecha_asignacion = formatDate(fechahoy)
        console.log(reporte)
        reporte.estado = "ASIGNADO"
        //console.log(Url + 'reportes/' + reporte.id)
        axios.put(Url + 'reportes/' + reporte.id, reporte, {
            headers: {
                Authorization : token,
            }
            })
            .then(res =>  {
                setReporte(res.data)
                closeModal()
                cargareportes()
                cerrarPanel()
            })
            .catch(err => console.log(err))
        
    }

    const onChangeInstrucciones = (event) => {
        setReporte(values => ({...values, ['instrucciones_mantenimiento']: event.target.value})) 
    }

    const styleModal = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '35%',
        height: '50%',
        bgcolor: 'background.paper',
        border: '2px solid #000',
        p: 4
    };

    const styleInputLabel = {
        width: '25%'
    }

    const styleTextField = {
        width: '65%'
    }

return(
        <div style={{ textAlign: "left" }}>
            <Box sx={{display:"flex", alignItems:"center", justifyContent:"center"}}>
                <div style={{marginRight:'3%'}}>
                    <Box sx={{height:36, width:36, color:'primary.main', border:1, borderRadius: '50%', ml:'-5%'}}>
                        <IconButton color="primary" onClick={() => mandarMaps(reporte)}>
                            <MapIcon fontSize="small"/>   
                        </IconButton>
                    </Box>
                    <p style={{color:blue[600], fontSize:'13px'}}>Maps</p>
                </div>
                <div style={{marginRight:'2%'}}>
                <Box sx={{height:36, width:36, color:'error.main', border:1, borderRadius: '50%', ml:'16%'}}>
                    <IconButton color="error" onClick={() => abrirSwalDelete(reporte.id)}>
                        <DeleteForeverIcon fontSize="small"/>   
                    </IconButton>
                </Box>
                <p style={{color:red[900], fontSize:'13px'}}>Rechazar</p>
                </div>
                <div>
                {!reporte.fecha_asignacion &&
                <><Box sx={{ height: 36, width: 36, color: 'success.main', border: 1, borderRadius: '50%', ml: '10%' }}>
                        <IconButton color="success"
                            onClick={() => abrirModal()}>
                            <DoneOutlineIcon fontSize="small" />
                        </IconButton>
                    </Box>
                    <p style={{ color: green[800], fontSize: '13px' }}>Asignar</p></>
                }
                </div>
            </Box>
            <Divider style={{marginTop: 10 }} />
            <Box className="BoxInfo">
                    <LightIcon fontSize="small" sx={{ marginRight: 1.3 }} /> 
                    <InputLabel sx={styleInputLabel}>PDL: </InputLabel>
                    <TextField multiline variant="standard" name="pdl" value={luminaria.pdl_id}
                     disabled placeholder="Ninguna" sx={styleTextField}/>
            </Box>
            <Box className="BoxInfo">
                <AlignHorizontalLeftIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                <InputLabel sx={styleInputLabel}>Latitud: </InputLabel>
                <TextField variant="standard" name="latitud" value={luminaria.latitud}
                   disabled sx={styleTextField}/>
            </Box>
            <Box className="BoxInfo">
                    <AlignVerticalBottomIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                    <InputLabel sx={styleInputLabel}>Longitud: </InputLabel>
                    <TextField variant="standard" name="longitud" value={luminaria.longitud}
                     disabled sx={styleTextField}/>
            </Box>
            <Box className="BoxInfo">
                    <DvrIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                    <InputLabel sx={styleInputLabel}>Estatus: </InputLabel>
                    <TextField variant="standard" name="longitud" value={reporte.estado}
                     disabled sx={styleTextField}/>
            </Box>
            {/*<Box className="BoxInfo">
                    <DvrIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                    <InputLabel sx={styleInputLabel}>Estatus: </InputLabel>
                    <FormControl size="small" 
                    sx={{ width: 150, paddingRight: '2%', paddingTop: '1.5%'}}>
                        <Select
                            onClose={() => {
                                setTimeout(() => {
                                document.activeElement.blur();
                                }, 0);
                            }}
                            id="select-estado"
                            value={age}
                            onChange={handleChange}
                            variant="standard"
                            >
                            <MenuItem value={"CREADO"}>             CREADO      </MenuItem>
                            <MenuItem value={"ASIGNADO"}>           ASIGNADO    </MenuItem>
                            <MenuItem value={"EN PROCESO"}>         EN PROCESO  </MenuItem>
                            <MenuItem value={"SOLUCIONADO"}>        SOLUCIONADO </MenuItem>
                            <MenuItem value={"TERMINADO"}>          TERMINADO   </MenuItem>
                        </Select>
                    </FormControl>
            </Box>
                        */}
            {reporte.fecha_asignacion !== null &&
            <Box className="BoxInfo">
                    <CalendarMonthIcon size="xl" style={{ marginRight: 6 }} />
                    <InputLabel sx={styleInputLabel}>Fecha Asignación: </InputLabel>
                    <TextField
                        sx={{width:150}}
                        variant="standard"
                        name="fecha_censo"
                        InputLabelProps={{ shrink: true, required: true }}
                        value={reporte.fecha_asignacion||""}
                        //onChange={handleChangeT}
                        disabled 
                        />
            </Box>
            }
            <Divider style={{marginBottom: 10 }} />
            <Modal 
             open={openModal}
             onClose={closeModal}>
                <Box sx={styleModal}>
                    <Box sx={{display:"flex", flexDirection:"column"}}>
                            <h3 style={{textAlign:"center"}}>Datos de Asignación</h3>
                        <div style={{display:"flex", alignItems:"center"}}>
                            Cuadrilla:
                            <FormControl size="small" 
                                sx={{ width: 200, paddingLeft: 1}}>
                                <Select
                                    onClose={() => {
                                        setTimeout(() => {
                                        document.activeElement.blur();
                                        }, 0);
                                    }}
                                    id="select-estado"
                                    value={cuadrilla}
                                    onChange={handleChange}
                                    variant="standard"
                                    MenuProps={MenuProps}
                                    >
                                    
                                    <MenuItem value={0} disabled>Seleccione cuadrilla</MenuItem>
                                    {cuadrilla2.map((cuadrilla, index) => (
                                        <MenuItem key={index} value={cuadrilla.id} style={{fontSize:"small"}}>
                                            {cuadrilla.nombre}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>
                        <br/>
                        <div style={{alignItems:"center"}}>
                            Intstrucciones de Mantenimiento:
                            <br/>
                            <TextField multiline maxRows={2} size="small" fullWidth={true}
                            onChange={onChangeInstrucciones}
                            placeholder="Instrucciones..." 
                            value={reporte.instrucciones_mantenimiento||""}/>
                        </div>
                        <br/>
                        <div style={{textAlign:"center"}}>
                            <Button color="success" variant="outlined" sx={{width:200}} onClick={asignacion}>
                                Confirmar Asiganción
                            </Button>
                        </div>
                    </Box>
                </Box>
            </Modal>
        </div>
    )

}

export default Reporte;