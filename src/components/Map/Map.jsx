import React, { useRef, useState, useEffect} from "react";
import mapboxgl from "mapbox-gl";
//import TopBar  from "../../layout/TopBar/TopBar";
import axios from 'axios'
import LoaderIndicator from '../../layout/LoaderIndicator/LoaderIndicator';
import EmojiObjectsIcon from '@mui/icons-material/EmojiObjects';

import "./Map.scss";
import classNames from "classnames";

import Panel from "../Panel Lateral/Panel";
import { Url } from "../../constants/global";
import { useNavigate } from "react-router-dom";

import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Luminaria from "../Luminaria/Luminaria";
import Comentarios from "../Comentarios/Comentario";

mapboxgl.accessToken = 'pk.eyJ1IjoibWlndWVsdHJhZmZpYyIsImEiOiJjbG01Z2U2cW0wajdiM3Bsb2N6ZGhrN2lxIn0.hMkzztmUbOf-N9uToXeBwA';

const Map = () => {
    const Navigate = useNavigate();
    //Variables que se estarán manipulando constantemente
    const token = localStorage.getItem('token');

    //Para el Mapa
    const mapContainer = useRef(null);
    const map = useRef(null);
    const [lat] = useState(19.0409511);
    const [lng] = useState(-98.221976);
    const [zoom] = useState(15);

    const [showPanel, setShowPanel] = useState(false);

    //Datos para Panel
    const [dataLuminaria, setdataLuminaria] = useState([]);
    const [dataComentarios, setdataComentarios] = useState([])
    const [ isCharging, setisCharging ] = useState(false);
    const [value, setValue] = React.useState('Informacion');

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    //Para puntero cuándo se cargue valores desde GET normal
    //const marker = new mapboxgl.Marker()

    //Método para asignar puntos personalizados desde método GET
    /*
    const updateMarker = useCallback( () => {

        axios.get('http://localhost:8081/2')
        .then(res =>  {

        res.data.forEach( (point) => {
            // Create a React ref
            const ref = React.createRef();
            // Create a new DOM node and save it to the React ref
            ref.current = document.createElement('div');
            // Render a Marker Component on our new DOM node
            createRoot(ref.current).render(
                <Marker onClick={markerClicked} data={point} />
            );

            // Create a Mapbox Marker at our new DOM node
            new mapboxgl.Marker(ref.current)
                .setLngLat([ point.longitud, point.latitud ])
                .addTo(map.current);
        } );
    })
    .catch(err => console.log(err))
    }, [markers]);*/


    useEffect(() => {
        
        if(token === null){
            Navigate("/")
        }else{
        //setIsLogging(true);
        if (map.current) return; // initialize map only once
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/navigation-night-v1',
            center: [lng, lat],
            zoom: zoom,
            boxZoom: true,
            
        });
                  
        map.current.on('load', function() {
            
            setisCharging(true);
            var url = Url + 'luminarias?proyecto=1';
            var headers = {
                'Authorization': token 
            };

            fetch(url, {
                method: 'GET',
                headers: headers
            })
            .then(response => response.json())
            .then(data => {
                setisCharging(false)

                map.current.addSource('xample_points', {
                type: 'geojson',
                data: data
                });

                map.current.addLayer({
                'id': 'xample_points',
                'type': 'circle',
                'source': 'xample_points',
               
                  'paint': {
                  'circle-radius': 4,
                  'circle-stroke-width': 0.5,
                  'circle-color': 'green',
                  'circle-stroke-color': 'white'
                  }  
                });
            })
            .catch(error => {
                setisCharging(false)
                console.error('Error al cargar el GeoJSON:', error);
            });
        
            //setIsLogging(false);
            const marker = new mapboxgl.Marker()

            map.current.on('mouseenter', 'xample_points', () => {
                map.current.getCanvas().style.cursor = 'pointer'
              })
              map.current.on('mouseleave', 'xample_points', () => {
                map.current.getCanvas().style.cursor = 'default'
              })

            map.current.on('click', 'xample_points', (e) => {

                const coordinates = e.features[0].geometry.coordinates;
                const description = e.features[0].properties;
                
                
                marker.setLngLat(coordinates)
                marker.addTo(map.current);

                markerClicked(description)
            });
           
        });
        }
        
    });

    const closePanel = () => {
        setShowPanel(false);
    }

    const markerClicked  = async(datos) => {
        setValue("Informacion")
        await axios.get(Url + 'luminarias/'+datos.id, {
            headers: {
                Authorization : token,
            }
          })
        .then(res =>  {
            setdataLuminaria(res.data);
          })
        .catch(err => console.log(err))

        axios.get(Url + 'luminarias/' + datos.id + '/comentarios', {
            headers: {
                Authorization : token,
            }
          })
        .then(res =>  {
            setdataComentarios(res.data);
          })
        .catch(err => console.log(err))

        setShowPanel(true);
    };

    return (           
       <div ref={mapContainer} className={classNames("map-container")}>
       {showPanel &&
        <Panel 
        dato={"PDL: " + dataLuminaria.pdl_id + " - " + dataLuminaria.id} 
        closePanel={closePanel} 
        icono={<EmojiObjectsIcon/>}
        top={0}>
            <TabContext value={value}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider'}}>
                    <TabList onChange={handleChange}>   
                        <Tab label="Información" value="Informacion" sx={{width:'33%'}}/>
                        <Tab label="Comentarios" value="Comentarios" sx={{width:'33%'}}/>
                        <Tab label="Brazos" value="Brazos" sx={{width:'33%'}}/>
                    </TabList>
                </Box>
                <TabPanel value="Informacion">
                    <Luminaria dataLuminariaPanel={dataLuminaria} />
                </TabPanel>
                <TabPanel value="Comentarios">
                    <Comentarios datacoments={dataComentarios} />
                </TabPanel>
                <TabPanel value="Brazos">
                    Componente Brazos
                </TabPanel>
            </TabContext>
        </Panel>
        }
        { isCharging && <LoaderIndicator /> }
        </div>
    );
}

export default Map;