import React, { useRef, useState, useEffect} from "react";
import mapboxgl from "mapbox-gl";
//import TopBar  from "../../layout/TopBar/TopBar";
import axios from 'axios'

import "./MapGeneral.scss";
import classNames from "classnames";

import { Url } from "../../constants/global";
import LoaderIndicator from "../../layout/LoaderIndicator/LoaderIndicator";

mapboxgl.accessToken = 'pk.eyJ1IjoibWlndWVsdHJhZmZpYyIsImEiOiJjbG01Z2U2cW0wajdiM3Bsb2N6ZGhrN2lxIn0.hMkzztmUbOf-N9uToXeBwA';

const MapGeneral = ({latitudReporte, longitudReporte, setinfoLuminaria, showButton, caso}) => {
    //Variables que se estarán manipulando constantemente
    const token = localStorage.getItem('token');

    //Para el Mapa
    const mapContainerGeneral = useRef(null);
    const map = useRef(null);
    const [lat] = useState(19.0409511);
    const [lng] = useState(-98.221976);
    const [zoom] = useState(17);
    let   [count] = useState(0);

    const [ isCharging, setisCharging ] = useState(false);

    useEffect(() => {

        //setIsLogging(true);
        switch (caso) {
            case 1:
                if (map.current) return; // initialize map only once
                map.current = new mapboxgl.Map({
                    container: mapContainerGeneral.current,
                    style: 'mapbox://styles/mapbox/navigation-night-v1',
                    center: [longitudReporte, latitudReporte],
                    zoom: zoom,
                    boxZoom: true
                });
                const marker1 = new mapboxgl.Marker({
                    color: "#BDBABA"
                    })
                marker1.setLngLat([longitudReporte, latitudReporte])
                marker1.addTo(map.current);
        
                map.current.on('load', function() {
                    var url = Url + 'luminarias?proyecto=1&latitud='+latitudReporte+'&longitud='+longitudReporte;
                    var headers = {
                        'Authorization': token 
                    };
        
                    setisCharging(true)
                    fetch(url, {
                        method: 'GET',
                        headers: headers
                    })
                    .then(response => response.json())
                    .then(data => {
                        setisCharging(false)
        
                        map.current.addSource('xample_points', {
                        type: 'geojson',
                        data: data
                        });
        
                        map.current.addLayer({
                        'id': 'xample_points',
                        'type': 'circle',
                        'source': 'xample_points',
                      
                          'paint': {
                          'circle-radius': 4,
                          'circle-stroke-width': 0.5,
                          'circle-color': 'green',
                          'circle-stroke-color': 'white'
                          }  
                        });
                    })
                    .catch(error => {
                        setisCharging(false)
                        console.error('Error al cargar el GeoJSON:', error);
                    });
        
                    //setIsLogging(false);
                    const marker = new mapboxgl.Marker()
        
                    map.current.on('mouseenter', 'xample_points', () => {
                        map.current.getCanvas().style.cursor = 'pointer'
                      })
                      map.current.on('mouseleave', 'xample_points', () => {
                        map.current.getCanvas().style.cursor = 'default'
                      })
        
                    map.current.on('click', 'xample_points', (e) => {
        
                        const coordinates = e.features[0].geometry.coordinates;
                        const description = e.features[0].properties;
                        
                        
                        marker.setLngLat(coordinates)
                        marker.addTo(map.current);
        
                        markerClicked(description)
                    });
                });
            break;

            case 2:
                if (map.current) return; // initialize map only once
                map.current = new mapboxgl.Map({
                    container: mapContainerGeneral.current,
                    style: 'mapbox://styles/mapbox/navigation-night-v1',
                    center: [lng, lat],
                    zoom: 14,
                    boxZoom: true
                });

                map.current.on('style.load', function() {

                    map.current.on('click', function(e) {
                      var coordinates = e.lngLat;
    
                      count = count + 1
          
                      if(count <= 1){
                      const marker1 = new mapboxgl.Marker({
                        color: "#BDBABA"
                        })
                        marker1.setLngLat(coordinates)
                        marker1.addTo(map.current);
                        //console.log(coordinates["lng"])
    
                        var url = Url + 'luminarias?proyecto=1&latitud='+coordinates["lat"]+'&longitud='+coordinates["lng"];
                        var headers = {
                            'Authorization': token 
                        };
            
                        fetch(url, {
                            method: 'GET',
                            headers: headers
                        })
                        .then(response => response.json())
                        .then(data => {
                            map.current.addSource('xample_points', {
                            type: 'geojson',
                            data: data
                            });
            
                            map.current.addLayer({
                            'id': 'xample_points',
                            'type': 'circle',
                            'source': 'xample_points',
                          
                              'paint': {
                              'circle-radius': 4,
                              'circle-stroke-width': 0.5,
                              'circle-color': 'green',
                              'circle-stroke-color': 'white'
                              }  
                            });
                        })
                        .catch(error => {
                            console.error('Error al cargar el GeoJSON:', error);
                        });
            
                        }
                        //setIsLogging(false);
                        const marker = new mapboxgl.Marker()
            
                        map.current.on('mouseenter', 'xample_points', () => {
                            map.current.getCanvas().style.cursor = 'pointer'
                          })
                          map.current.on('mouseleave', 'xample_points', () => {
                            map.current.getCanvas().style.cursor = 'default'
                          })
            
                        map.current.on('click', 'xample_points', (e) => {
            
                            const coordinates = e.features[0].geometry.coordinates;
                            const description = e.features[0].properties;
                            
                            
                            marker.setLngLat(coordinates)
                            marker.addTo(map.current);
            
                            markerClicked(description)
                        });
    
                    });
                  });
            break;
        
            default:
                break;
        }
        
        
    });

    const markerClicked  = async (datos) => {
        await axios.get(Url + 'luminarias/'+datos.id, {
            headers: {
                Authorization : token,
            }
          })
        .then(res =>  {
            setinfoLuminaria(res.data);
            showButton(true);
          })
        .catch(err => console.log(err))
    };

    return (           
       <div ref={mapContainerGeneral} className={classNames("map-containerGeneral")}>
        { isCharging && <LoaderIndicator /> }
       </div>
    );
}

export default MapGeneral;