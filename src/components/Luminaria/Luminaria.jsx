import React, { useRef, useState, useEffect, useCallback } from "react";
import axios from 'axios'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

//Iconos de la sección Información
import ListItemIcon from '@mui/material/ListItemIcon';
import HeightIcon from '@mui/icons-material/Height';
import HardwareIcon from '@mui/icons-material/Hardware';
import VillaIcon from '@mui/icons-material/Villa';
import InputAdornment from '@mui/material/InputAdornment';
import TroubleshootIcon from '@mui/icons-material/Troubleshoot';
import BuildIcon from '@mui/icons-material/Build';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import CableIcon from '@mui/icons-material/Cable';
import Checkbox from '@mui/material/Checkbox';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import AlignHorizontalLeftIcon from '@mui/icons-material/AlignHorizontalLeft';
import AlignVerticalBottomIcon from '@mui/icons-material/AlignVerticalBottom';
import EmojiObjectsIcon from '@mui/icons-material/EmojiObjects';
import HomeWorkOutlinedIcon from '@mui/icons-material/HomeWorkOutlined';
import HomeWorkIcon from '@mui/icons-material/HomeWork';

import TextField from '@mui/material/TextField';

//Icono deshabilitar Edición
import Switch from '@mui/material/Switch';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faRoad } from '@fortawesome/free-solid-svg-icons'

import InputLabel from '@mui/material/InputLabel';
import './Luminaria.scss';


import { Url } from "../../constants/global";

const Luminaria = ({dataLuminariaPanel}) => {
    //Variables que se estarán manipulando constantemente
    const token = localStorage.getItem('token');

    //Para los datos de la luminaria
    const [dataLuminaria, setdataLuminaria] = useState([])

    //checkEtiquetadoBoxs
    const [checkEtiquetado, setcheckEtiquetado] = useState(null);
    const [checkUrbanizado, setcheckUrbanizado] = useState(null);

    if (dataLuminaria.id !== dataLuminariaPanel.id) {
        setdataLuminaria(dataLuminariaPanel)
        setcheckEtiquetado(dataLuminariaPanel.etiquetado)
        setcheckUrbanizado(dataLuminariaPanel.urbanizado)
    }

    const styleLabel = {
        width: '27.5%',
    }

    const styleTextfield = {
        fontsize: 40,
        width: '80%',
    }

    //Para valores true/false
    const [estadoSwitch, setestadoSwitch] = React.useState(false);
    const [deshabilitar, setdeshabilitar] = useState(true);


    //Obtener y mostrar los valores de los campos TextFields del formulario Información
    const handleChangeT = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        
        setdataLuminaria(values => ({...values, [name]: value}))
      }

    //Acceder y setear el valor del checkEtiquetadobox 
    const onChangeC = (event) => {
        const tipocheckEtiquetado = (event.target.name)
        switch (tipocheckEtiquetado) {
            case "etiquetado":
                setcheckEtiquetado(!checkEtiquetado)
                setdataLuminaria(values => ({...values, ['etiquetado']: !checkEtiquetado})) 
                break;
            case "urbanizado":
                setcheckUrbanizado(!checkUrbanizado)
                setdataLuminaria(values => ({...values, ['urbanizado']: !checkUrbanizado})) 
                break;
        }
        
    }

    const onChangeSwitch = () => {
        setestadoSwitch(!estadoSwitch);
        setdeshabilitar(!deshabilitar);
    } 

    //Método reservado para PUT de información
    const enviarDatos = () => {
        axios.put(Url + 'luminarias/'+dataLuminaria.id, dataLuminaria ,{
            headers: {
                Authorization : token,
            }
          })
        .then(res =>  {
            setdataLuminaria(res.data)
            setcheckEtiquetado(res.data.etiquetado)
            setcheckUrbanizado(res.data.urbanizado)
            setdeshabilitar(!deshabilitar)
            setestadoSwitch(!estadoSwitch)
          })
        .catch(err => console.log(err))
    }

    return (         
            <>
            <div style={{ textAlign: "right", display: "flex", flexDirection: "row-reverse" }}>
            <Switch
                checked={estadoSwitch}
                onChange={onChangeSwitch}
                color="success" />
            <Typography sx={{ marginTop: '1.5%' }}>Habilitar Edición:</Typography>
            </div>
            <div style={{ textAlign: "left" }}>
                <Box className="BoxInfoLuminaria">
                    <EmojiObjectsIcon fontSize="small" sx={{ marginRight: 1.3, marginBottom: 0.3 }} />
                    <InputLabel sx={styleLabel} >PDL: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="pdl_id" value={dataLuminaria.pdl_id} 
                    disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <AlignHorizontalLeftIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                    <InputLabel sx={styleLabel} >Latitud: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="latitud" value={dataLuminaria.latitud} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <AlignVerticalBottomIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                    <InputLabel sx={styleLabel} >Longitud: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="longitud" value={dataLuminaria.longitud} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <HeightIcon sx={{ marginRight: 1 }} />
                    <InputLabel sx={styleLabel} >Altura:</InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="altura" value={dataLuminaria.altura} disabled={deshabilitar}
                        onChange={handleChangeT}
                        InputProps={{
                            //readOnly: true,
                            endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                        }} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <BuildIcon fontSize="small" sx={{ marginRight: 1.3 }} />
                    <InputLabel sx={styleLabel} >Tipo Poste: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="tipo_poste" value={dataLuminaria.tipo_poste} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <HardwareIcon sx={{ marginRight: 1 }} />
                    <InputLabel sx={styleLabel} >Material: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="material" value={dataLuminaria.material} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <TroubleshootIcon sx={{ marginRight: 1 }} />
                    <InputLabel sx={styleLabel} >Condición: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="condicion" value={dataLuminaria.condicion} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <CableIcon sx={{ marginRight: 1 }} />
                    <InputLabel sx={styleLabel} >Cable Electrico: </InputLabel>
                    <TextField sx={styleTextfield}variant="standard" name="condicion_cable_electrico" value={dataLuminaria.condicion_cable_electrico} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <FontAwesomeIcon icon={faRoad} size="xl" style={{ marginRight: 9 }} />
                    <InputLabel sx={styleLabel} >Calle: </InputLabel>
                    <TextField sx={styleTextfield} multiline variant="standard" name="calle" value={dataLuminaria.calle} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <VillaIcon sx={{ marginRight: 1 }} />
                    <InputLabel sx={styleLabel} >Colonia: </InputLabel>
                    <TextField sx={styleTextfield} multiline variant="standard" name="colonia" value={dataLuminaria.colonia} disabled={deshabilitar}
                        onChange={handleChangeT} />
                </Box>
                <Box className="BoxInfoLuminaria">
                    <CalendarMonthIcon sx={{ marginRight: 1 }} />
                    <InputLabel sx={styleLabel} >Fecha Censo: </InputLabel>
                    <TextField
                        sx={styleTextfield}
                        variant="standard"
                        name="fecha_censo"
                        InputLabelProps={{ shrink: true, required: true }}
                        type="date"
                        value={dataLuminaria.fecha_censo}
                        onChange={handleChangeT}
                        disabled={deshabilitar} />
                </Box>
            </div>
            <Divider style={{ marginBottom: 6, marginTop: 10 }} />
            <Box sx={{ display: 'flex', alignItems: 'flex-start', marginTop: 1 }}>
                <InputLabel sx={{ marginTop: 1 }}>Etiquetado: </InputLabel>
                <Checkbox
                    name="etiquetado"
                    icon={<BookmarkBorderIcon />}
                    checkedIcon={<BookmarkIcon />}
                    checked={checkEtiquetado}
                    onChange={onChangeC}
                    disabled={deshabilitar} />

                <InputLabel sx={{ marginLeft: 4, marginTop: 1 }}>Urbanizado: </InputLabel>
                <Checkbox
                    name="urbanizado"
                    icon={<HomeWorkOutlinedIcon />}
                    checkedIcon={<HomeWorkIcon />}
                    checked={checkUrbanizado}
                    onChange={onChangeC}
                    disabled={deshabilitar} />
            </Box>
            {!deshabilitar &&
                <><Divider style={{marginTop: 10 }} />
                <div style={{ textAlign: "right" }}>
                    <Button variant="outlined" color="success"
                    sx={{ marginTop: 1 }}
                    onClick={enviarDatos}>
                    GUARDAR
                    </Button>
                </div></>
            }
            </>
    );
}

export default Luminaria;